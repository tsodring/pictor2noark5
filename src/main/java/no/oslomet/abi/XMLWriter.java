package no.oslomet.abi;

import no.oslomet.abi.model.noark.VSM;
import no.oslomet.abi.model.noark.*;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static no.oslomet.abi.utils.Constants.*;

public class XMLWriter {

    XMLStreamWriter streamWriter;
    int sequenceCount = 1;

    public XMLWriter(String location) throws IOException, XMLStreamException {
        if (!location.endsWith(File.separator))
            location += File.separator;
        streamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(
                new FileWriter(location + "arkivstruktur.xml"));
        streamWriter.writeStartDocument("utf-8", "1.0");
    }

    /**
     * Starts a single fonds element. Note there is no support for subfonds with
     * this approach.
     *
     * @throws XMLStreamException if a problem occurs
     */
    public void openFonds(Fonds fonds) throws XMLStreamException {
        streamWriter.writeStartElement(FONDS);
        streamWriter.writeNamespace("xsi", XSI);
        streamWriter.writeDefaultNamespace(FONDS_STRUCTURE_NAMESPACE);
        streamWriter.writeNamespace("n5mdk", MDK_NAMESPACE);
        streamWriter.writeNamespace("tab", TAB_NAMESPACE);
        streamWriter.writeAttribute("xsi:schemaLocation", SCHEMA_LOCATION +
                " " + TAB_SCHEMA_LOCATION);
    }

    /**
     * Closes the single fonds object in the XML document. Note this also
     * flushes and closes the XML document as there is nothing left to do
     * after this.
     *
     * @throws XMLStreamException if a problem occurs
     */
    public void closeFonds() throws XMLStreamException {
        streamWriter.writeEndElement();
        streamWriter.writeEndDocument();
        streamWriter.flush();
        streamWriter.close();
    }

    public void writeFonds(Fonds fonds)
            throws XMLStreamException {
        writeSystemId(fonds);
        writeTitleAndDescription(fonds);
        writeElement(FONDS_STATUS, fonds.getFondsStatus());
        writeCreated(fonds);
        writeFinalised(fonds);
    }

    public void writeSeries(Series series)
            throws XMLStreamException {
        writeSystemId(series);
        writeTitleAndDescription(series);
        writeElement(SERIES_STATUS, series.getSeriesStatus());
        writeCreated(series);
        writeFinalised(series);
    }

    public void writeFondsCreator(FondsCreator fondsCreator)
            throws XMLStreamException {
        openComplexType(FONDS_CREATOR);
        writeElement(FONDS_CREATOR_ID, fondsCreator.getFondsCreatorId());
        writeElement(FONDS_CREATOR_NAME, fondsCreator.getFondsCreatorName());
        writeElement(DESCRIPTION, fondsCreator.getDescription());
        closeComplexType();
    }

    private void writeSystemId(NoarkEntity entity)
            throws XMLStreamException {
        writeElement(SYSTEM_ID, entity.getSystemId());
    }

    private void writeTitleAndDescription(ITitleAndDescription entity)
            throws XMLStreamException {
        writeElement(TITLE, entity.getTitle());
        if (null != entity.getDescription()) {
            writeElement(DESCRIPTION, entity.getDescription());
        }
    }

    private void writeCreated(NoarkEntity entity)
            throws XMLStreamException {
        if (null != entity.getCreatedDate()) {
            writeElement(CREATED_DATE, entity.getCreatedDate()
                    .format(ISO_OFFSET_DATE_TIME));
        }
        writeElement(CREATED_BY, entity.getCreatedBy());
    }

    private void writeFinalised(IFinalised entity)
            throws XMLStreamException {
        if (null != entity.getFinalisedDate()) {
            writeElement(FINALISED_DATE, entity.getFinalisedDate()
                    .format(ISO_OFFSET_DATE_TIME));
        }
        writeElement(FINALISED_BY, entity.getFinalisedBy());
    }

    public void writeElement(String name, String value)
            throws XMLStreamException {
        streamWriter.writeStartElement(name);
        streamWriter.writeCharacters(value);
        streamWriter.writeEndElement();
    }

    /*
    Example <tab:negativ>
 It the value is null then the element is not written. Note. All values are
 subjected to a trim function to remove whitespace.
     */
    public void writeElementWithNamespacePrefix(
            String name, String value, String prefix)
            throws XMLStreamException {
        if (value != null && !value.isEmpty()) {
            streamWriter.writeStartElement(prefix + ":" + name);
            streamWriter.writeCharacters(value.stripLeading().stripTrailing());
            streamWriter.writeEndElement();
        }
    }

    public void openComplexType(String name)
            throws XMLStreamException {
        streamWriter.writeStartElement(name);
    }

    public void closeComplexType()
            throws XMLStreamException {
        streamWriter.writeEndElement();
    }

    public void writeFile(no.oslomet.abi.model.noark.File file)
            throws XMLStreamException {
        writeSystemId(file);
        writeElement(FILE_ID, file.getFileId());
        writeTitleAndDescription(file);
        writeCreated(file);
        writeFinalised(file);
    }

    public void writeRecordPart1(Record record) throws XMLStreamException {
        writeSystemId(record);
        writeCreated(record);
        writeElement(ARCHIVED_DATE, record.getArchivedDate()
                .format(ISO_OFFSET_DATE_TIME));
        writeElement(ARCHIVED_BY, record.getArchivedBy());
    }

    public void writeRecordPart2(Record record) throws XMLStreamException {
        writeElement(RECORD_ID, record.getRecordId());
        writeTitleAndDescription(record);
    }

    public void writeVSM(VSM vsm) throws XMLStreamException {
        openComplexType(VSM);
        openComplexType(TAM_PREFIX + ":" + TAM_BILDE);
        writeElementWithNamespacePrefix(TAM_ARKIVBILDARE, vsm.getArkivbildare(),
                TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_BILDNUMMER, vsm.getBildnummer(),
                TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_NEGATIV,
                vsm.getNegativ().toString(), TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_PLATS, vsm.getPlats(), TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_AR, vsm.getAr(), TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_MOTIV, vsm.getMotiv(), TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_PUBLIKATION, vsm.getPublikation(),
                TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_BILDRATTIGHETER,
                vsm.getBildrattigheter(), TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_ANMARKNINGAR, vsm.getAnmarkningar(),
                TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_PLACERING, vsm.getPlacering(),
                TAM_PREFIX);
        writeElementWithNamespacePrefix(TAM_INDEX, vsm.getIndex().toString(),
                TAM_PREFIX);
        closeComplexType();
        closeComplexType();
    }

    public void writeDocumentDescription(
            DocumentDescription documentDescription) throws XMLStreamException {
        writeSystemId(documentDescription);
        writeElement(DOCUMENT_TYPE_CODENAME,
                documentDescription.getDocumentTypeCodeName());
        writeElement(DOCUMENT_STATUS, documentDescription.getDocumentStatus());
        writeTitleAndDescription(documentDescription);
        writeCreated(documentDescription);
        writeElement(ASSOCIATED_WITH_RECORD_AS_CODENAME,
            documentDescription.getAssociatedWithRecordAsCodeName());
        writeElement(DOCUMENT_NUMBER,
                documentDescription.getDocumentNumber().toString());
        writeElement(ASSOCIATED_WITH_RECORD_DATE,
            documentDescription.getAssociatedDate()
                    .format(ISO_OFFSET_DATE_TIME));
        writeElement(ASSOCIATED_BY, documentDescription.getAssociatedBy());
    }

    public void writeDocumentObject(DocumentObject documentObject)
            throws XMLStreamException {
        writeSystemId(documentObject);
        writeElement(VERSION_NUMBER,
                documentObject.getVersionNumber().toString());
        writeElement(VARIANT_FORMAT_CODE_NAME,
                documentObject.getVariantFormatCodeName());
        writeElement(FORMAT, documentObject.getFormat());
        writeCreated(documentObject);
        writeElement(REFERENCE_DOCUMENT_FILE, documentObject.
                getReferenceDocumentFile());
        writeElement(CHECKSUM, documentObject.getChecksum());
        writeElement(CHECKSUM_ALGORITHM, documentObject.getChecksumAlgorithm());
        writeElement(FILE_SIZE, documentObject.getFileSize().toString());
    }

    public void openComplexTypeBase(String elementName, String type)
            throws XMLStreamException {
        streamWriter.writeStartElement(elementName);
        streamWriter.writeAttribute("xsi:type", type);
    }
}
