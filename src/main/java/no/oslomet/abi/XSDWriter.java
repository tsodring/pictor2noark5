package no.oslomet.abi;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static no.oslomet.abi.utils.Constants.*;

/**
 * Write the file tambilder.xsd
 */
public class XSDWriter {

    XMLStreamWriter streamWriter;

    public XSDWriter(String location) throws IOException, XMLStreamException {
        if (!location.endsWith(File.separator))
            location += File.separator;
        streamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(
                new FileWriter(location + TAM_XSD_FILENAME + ".xsd"));
        streamWriter.writeStartDocument("utf-8", "1.0");
    }

    public void close() throws XMLStreamException {
        streamWriter.flush();
        streamWriter.close();
    }

    /**
     * Create an XSD file capable of describing the TAM metadata
     *
     * @throws XMLStreamException if a problem occurs
     */
    public void writeVSMXSD()
            throws XMLStreamException {
        // <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
        //   xmlns="http://www.tam-arkiv.se/bilder"
        //   targetNamespace="http://www.tam-arkiv.se/bilder"
        //   version="1.0">
        streamWriter.setPrefix("xs", XS);
        streamWriter.writeStartElement(XS, "schema");
        streamWriter.writeNamespace("xs", XS);
        streamWriter.writeDefaultNamespace(TAB_NAMESPACE);
        streamWriter.writeNamespace("tab", TAB_NAMESPACE);
        streamWriter.writeAttribute("targetNamespace", TAB_NAMESPACE);
        streamWriter.writeAttribute("elementFormDefault", "qualified");
        streamWriter.writeAttribute("version", "1.0");
        //  <xs:element name="bilde" type="bilde"/>
        streamWriter.writeStartElement(XS, "element");
        streamWriter.writeAttribute("name", TAM_BILDE);
        streamWriter.writeAttribute("type", TAM_BILDE);
        streamWriter.writeEndElement();
        // <xs:complexType name="bilde">
        streamWriter.writeStartElement(XS, "complexType");
        streamWriter.writeAttribute("name", TAM_BILDE);
        //  <xs:sequence>
        streamWriter.writeStartElement(XS, "sequence");
        createElement(streamWriter, TAM_ARKIVBILDARE, "xs:string");
        createElement(streamWriter, TAM_BILDNUMMER, "xs:string");
        createElement(streamWriter, TAM_NEGATIV, "xs:boolean");
        createElement(streamWriter, TAM_PLATS, "xs:string");
        createElement(streamWriter, TAM_AR, "xs:string");
        createElement(streamWriter, TAM_MOTIV, "xs:string");
        createElement(streamWriter, TAM_PUBLIKATION, "xs:string");
        createElement(streamWriter, TAM_BILDRATTIGHETER, "xs:string");
        createElement(streamWriter, TAM_ANMARKNINGAR, "xs:string");
        createElement(streamWriter, TAM_PLACERING, "xs:string");
        createElement(streamWriter, TAM_INDEX, "xs:integer");
        //  </xs:sequence>
        streamWriter.writeEndElement();
        // </xs:complexType>
        streamWriter.writeEndElement();
        // </xs:schema>
        streamWriter.writeEndElement();
    }

    private void createElement(XMLStreamWriter streamWriter,
                               String elementName, String dataType)
            throws XMLStreamException {
        streamWriter.writeStartElement(XS, "element");
        streamWriter.writeAttribute("name", elementName);
        streamWriter.writeAttribute("type", dataType);
        streamWriter.writeAttribute("minOccurs", "0");
        streamWriter.writeEndElement();
    }
}
