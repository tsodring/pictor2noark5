package no.oslomet.abi.model.noark;
import no.oslomet.abi.model.noark.utils.DateTimeAdapter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.OffsetDateTime;
import java.util.UUID;

public class NoarkEntity {

    /**
     * M001 - systemID (xs:string)
     */
    @XmlElement(name = "systemID", required = true)
    private UUID systemId = UUID.randomUUID();

    /**
     * M600 - opprettetDato (xs:dateTime)
     */
    @XmlElement(name = "opprettetDato", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private OffsetDateTime createdDate = OffsetDateTime.now();

    /**
     * M601 - opprettetAv (xs:string)
     */
    @XmlElement(name = "opprettetAv", required = true)
    private String createdBy;

    public String getSystemId() {
        if (null != systemId)
            return systemId.toString();
        else
            return null;
    }

    public void setSystemId(UUID systemId) {
        this.systemId = systemId;
    }

    public String getId() {
        return systemId.toString();
    }

    public void setId(UUID systemId) {
        this.systemId = systemId;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(systemId)
                .append(createdDate)
                .append(createdBy)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        NoarkEntity rhs = (NoarkEntity) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(systemId, rhs.getSystemId())
                .append(createdDate, rhs.getSystemId())
                .append(createdBy, rhs.getSystemId())
                .isEquals();
    }

    @Override
    public String toString() {
        return "NoarkEntity{" +
                "systemId=" + systemId +
                ", createdDate=" + createdDate +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
