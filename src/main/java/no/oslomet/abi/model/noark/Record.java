package no.oslomet.abi.model.noark;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.OffsetDateTime;

public class Record
        extends NoarkGeneralEntity {

    /**
     * M050 - arkivstatus code name (xs:string)
     */
    private String recordId;

    /**
     * M300 - dokumentmedium code name (xs:string)
     */
    private String documentMedium;

    private OffsetDateTime archivedDate = OffsetDateTime.now();

    private String archivedBy;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getDocumentMedium() {
        return documentMedium;
    }

    public void setDocumentMedium(String documentMedium) {
        this.documentMedium = documentMedium;
    }

    public OffsetDateTime getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(OffsetDateTime archivedDate) {
        this.archivedDate = archivedDate;
    }

    public String getArchivedBy() {
        return archivedBy;
    }

    public void setArchivedBy(String archivedBy) {
        this.archivedBy = archivedBy;
    }

    @Override
    public String toString() {
        return "Fonds{" + super.toString() +
                ", recordId='" + recordId + '\'' +
                ", documentMedium='" + documentMedium + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != getClass()) {
            return false;
        }
        Record rhs = (Record) other;
        return new EqualsBuilder()
                .appendSuper(super.equals(other))
                .append(recordId, rhs.recordId)
                .append(documentMedium, rhs.documentMedium)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(recordId)
                .append(documentMedium)
                .toHashCode();
    }
}
