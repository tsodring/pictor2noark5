package no.oslomet.abi.model.noark.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter
        extends XmlAdapter<String, Date> {

    private final DateFormat dateFormat =
            new SimpleDateFormat("dd-MM-yyyyZ");

    @Override
    public Date unmarshal(String xml) throws Exception {
        return dateFormat.parse(xml);
    }

    @Override
    public String marshal(Date object){
        return dateFormat.format(object);
    }
}
