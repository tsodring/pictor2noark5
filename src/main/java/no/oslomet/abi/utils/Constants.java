package no.oslomet.abi.utils;

public final class Constants {
    public static final String SYSTEM_ID = "systemID";
    public static final String CREATED_BY = "opprettetAv";
    public static final String CREATED_DATE = "opprettetDato";
    public static final String FINALISED_BY = "avsluttetAv";
    public static final String FINALISED_DATE = "avsluttetDato";
    public static final String TITLE = "tittel";
    public static final String DESCRIPTION = "beskrivelse";
    public static final String FONDS_CREATOR_ID = "arkivskaperID";
    public static final String FONDS_CREATOR_NAME = "arkivskaperNavn";
    public static final String VERSION_NUMBER = "versjonsnummer";
    public static final String FORMAT = "format";
    public static final String VARIANT_FORMAT_CODE_NAME = "variantformat";
    public static final String CHECKSUM = "sjekksum";
    public static final String CHECKSUM_ALGORITHM = "sjekksumAlgoritme";
    public static final String DOCUMENT_TYPE_CODENAME = "dokumenttype";
    public static final String DOCUMENT_STATUS = "dokumentstatus";
    public static final String ASSOCIATED_WITH_RECORD_AS_CODENAME =
            "tilknyttetRegistreringSom";
    public static final String ASSOCIATED_WITH_RECORD_DATE = "tilknyttetDato";
    public static final String ASSOCIATED_BY = "tilknyttetAv";
    public static final String DOCUMENT_NUMBER = "dokumentnummer";
    public static final String FILE_ID = "mappeID";
    public static final String RECORD_ID = "registreringsID";
    public static final String ARCHIVED_DATE = "arkivertDato";
    public static final String ARCHIVED_BY = "arkivertAv";

    public static final String FONDS = "arkiv";
    public static final String FONDS_CREATOR = "arkivskaper";
    public static final String SERIES = "arkivdel";
    public static final String FILE = "mappe";
    public static final String RECORD = "registrering";
    public static final String VSM = "virksomhetsspesifikkeMetadata";
    public static final String DOCUMENT_DESCRIPTION = "dokumentbeskrivelse";
    public static final String DOCUMENT_OBJECT = "dokumentobjekt";
    public static final String FINALISED = "Avsluttet";
    public static final String REFERENCE_DOCUMENT_FILE = "referanseDokumentfil";
    public static final String FILE_SIZE = "filstoerrelse";

    public static final String SERIES_STATUS = "arkivdelstatus";
    public static final String FONDS_STATUS = "arkivstatus";

    public static final String TAM_ARKIVBILDARE = "arkivbildare";
    public static final String TAM_BILDE = "bilde";
    public static final String TAM_BILDNUMMER = "bildnummer";
    public static final String TAM_NEGATIV = "negativ";
    public static final String TAM_PLATS = "plats";
    public static final String TAM_AR = "ar";
    public static final String TAM_MOTIV = "motiv";
    public static final String TAM_PUBLIKATION = "publikation";
    public static final String TAM_BILDRATTIGHETER = "bildrattigheter";
    public static final String TAM_ANMARKNINGAR = "anmarkningar";
    public static final String TAM_PLACERING = "placering";
    public static final String TAM_INDEX = "index";
    public static final String TAM_PREFIX = "tab";

    public static final String DOCUMENT_DIRECTORY = "DOKUMENT";
    public static final String TAM_XSD_FILENAME = "tambilder";

    public static final String XSI =
            "http://www.w3.org/2001/XMLSchema-instance";
    public static final String XS =
            "http://www.w3.org/2001/XMLSchema";
    public static final String FONDS_STRUCTURE_NAMESPACE =
            "http://www.arkivverket.no/standarder/noark5/arkivstruktur";
    public static final String MDK_NAMESPACE
            = "http://www.arkivverket.no/standarder/noark5/metadatakatalog";
    public static final String TAB_NAMESPACE
            = "http://www.tam-arkiv.se/bilder";
    public static final String SCHEMA_LOCATION =
            "http://www.arkivverket.no/standarder/noark5/arkivstruktur " +
                    "arkivstruktur.xsd";
    public static final String TAB_SCHEMA_LOCATION =
            "http://www.tam-arkiv.se/bilder tambilder.xsd";
    //public static final String  = "";
}
