package no.oslomet.abi;

import org.apache.commons.cli.*;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Pictor2XML {

    public static void main(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption(
                Option.builder()
                        .longOpt("file")
                        .desc("Location of MS access file")
                        .hasArg(true)
                        .numberOfArgs(1)
                        .required(true)
                        .build());
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        if (cmd.hasOption("file")) {
            try {
                XMLWriter xmlWriter = new XMLWriter("./");
                XSDWriter xsdWriter = new XSDWriter("./");
                MDBReader reader = new MDBReader(
                        cmd.getOptionValue("file"), xmlWriter, xsdWriter);
                reader.printColumns();
                reader.processData();
                xsdWriter.close();
            } catch (IOException | XMLStreamException e) {
                System.out.println("Error: Problem with access file [" +
                        cmd.getOptionValue("file") + "]. " +
                        e.getMessage());
            }
        }
        else {
            System.out.println("Error: missing access file location");
        }
    }
}
