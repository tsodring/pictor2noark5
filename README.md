# pictor2Noark5

A simple application that turns the contents of the Pictor database to an arkivstruktur.xml file. The application is a maven application so all project
dependencies are handled in pom.xml.

## Building the application
Pictor2Noark5 is developed with Java 11 and Apache Maven 3.6.0. The application can be built by running the following command in the root of the source:

    mvn package

## Running the application
Once the application has been built it can be run using:

     mvn exec:java -Dexec.arguments="-file/path/to/accesfile.db"

## Structure 

There are two tables in the database:
 1. TabArkiv
 2. TabBild

TabArkiv has rows and seems to describe various archives. There is no relationship
between TabArkiv and TabBild. TabBild contains metadata about all the pictures. The
actual pictures are stored external to the Access database.

The following are a list of columns in the Access file:

	 Arkivbildare (TEXT)
	 Bildnummer (TEXT)
	 Negativ (BOOLEAN)
	 Plats (TEXT)
	 Ar (TEXT)
	 Motiv (MEMO)
	 Publikation (TEXT)
	 Bildrattigheter (TEXT)
	 Anmarkningar (TEXT)
	 Placering (TEXT)
	 Index (LONG)
    
